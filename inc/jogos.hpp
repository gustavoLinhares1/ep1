#ifndef JOGOS_HPP
#define JOGOS_HPP

#include <string>
#include "quadros.hpp"

using namespace std;

class jogos : public quadros{
private:
  string plataforma;
  string personagem;
public:
  jogos();
  jogos(float valor, string tamanho, string nome, int quantidade,string categoria,string plataforma ,string personagem);
  ~jogos();
  void set_plataforma(string plataforma);
  string get_plataforma();
  void set_personagem(string personagem);
  string get_personagem();
  void imprime_quadro();
};
#endif
