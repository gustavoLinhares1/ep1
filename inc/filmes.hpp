#ifndef FILMES_HPP
#define FILMES_HPP

#include <string>
#include "quadros.hpp"

using namespace std;

class filmes : public quadros{
private:
  string ator;
  string genero;
public:
  filmes();
  filmes(float valor, string tamanho, string nome, int quantidade,string categoria,string ator ,string genero);
  ~filmes();
  void set_ator(string ator);
  string get_ator();
  void set_genero(string genero);
  string get_genero();
  void imprime_quadro();
};
#endif
