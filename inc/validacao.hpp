#ifndef VALIDACAO_HPP
#define VALIDACAO_HPP

#include <string>

using namespace std;

class Validacao_dados{
public:
  Validacao_dados();
  ~Validacao_dados();
  string valida_string(string string_valida);
  int valida_int(int inteiro_valido);
  float valida_float(float float_valido);
};
#endif
