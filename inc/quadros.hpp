#ifndef QUADROS_HPP
#define QUADROS_HPP

#include <string>

using namespace std;

class quadros{
private:
  int quantidade;
  float valor;
  string tamanho;
  string nome;
  string categoria;
public:
  quadros();
  quadros(float valor, string tamanho, string nome, int quantidade, string categoria);
  ~quadros();
  void set_quantidade(int quantidade);
  int get_quantidade();
  void set_valor(float valor);
  float get_valor();
  void set_tamanho(string tamanho);
  string get_tamanho();
  void set_nome(string nome);
  string get_nome();
  void set_categoria(string categoria);
  string get_categoria();
  virtual void imprime_quadro();
};
#endif
