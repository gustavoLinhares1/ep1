#include "validacao.hpp"
#include <iostream>
#include <string>
#include <vector>


Validacao_dados::Validacao_dados(){
  // cout << "construtor da classe de validação de dados" << endl;
}
Validacao_dados::~Validacao_dados(){
  // cout << "destrutor da classe de validação de dados" << endl;
}

string Validacao_dados::valida_string(string string_valida){
  getline(cin, string_valida);
  while(string_valida.empty()){
    cout << "Texto vazio. Insira novamente :";
    getline(cin, string_valida);
  }
  return string_valida;
}

int Validacao_dados::valida_int(int inteiro_valido){
  cin >> inteiro_valido;
  while(cin.fail()){
    cin.clear();
    cin.ignore(32767,'\n');
    cout << "Entrada inválida! Insira novamente: " << endl;
    cin >> inteiro_valido;
  }
  cin.ignore(32767,'\n');
  return inteiro_valido;
}


float Validacao_dados::valida_float(float float_valido){
  cin >> float_valido;
  while(cin.fail()){
    cin.clear();
    cin.ignore(32767,'\n');
    cout << "Entrada inválida! Insira novamente: " << endl;
    cin >> float_valido;
  }
  cin.ignore(32767,'\n');
  return float_valido;
}
