#include "jogos.hpp"
#include <iostream>
#include <string>
#include <vector>

jogos::jogos(){
  plataforma ="Sem plataforma";
  personagem ="Sem personagem";
}

jogos::jogos(float valor, string tamanho, string nome, int quantidade,string categoria,string plataforma ,string personagem){
  set_valor(valor);
  set_tamanho(tamanho);
  set_nome(nome);
  set_quantidade(quantidade);
  set_categoria(categoria);
  set_plataforma(plataforma);
  set_personagem(personagem);
}

jogos::~jogos(){

}
void jogos::set_plataforma(string plataforma){
  this->plataforma = plataforma;
}
string jogos::get_plataforma(){
  return plataforma;
}
void jogos::set_personagem(string personagem){
  this->personagem = personagem;
}
string jogos::get_personagem(){
  return personagem;
}

void jogos::imprime_quadro(){
  cout << "Nome: "<< get_nome() << endl;
  cout << "Quantidade: "<< get_quantidade() << endl;
  cout << "Tamanho: "<< get_tamanho() << " cm" << endl;
  cout << "Preço: "<< get_valor() <<" R$"<< endl;
  cout << "Categorias: "<< get_categoria() << endl;
  cout << "Personagem: "<< get_personagem() << endl;
  cout << "Plataforma: "<< get_plataforma() << endl;

}
