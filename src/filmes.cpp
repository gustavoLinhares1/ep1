#include "filmes.hpp"
#include <iostream>
#include <string>
#include <vector>

filmes::filmes(){
  ator ="Sem ator";
  genero ="Sem genero";
}

filmes::filmes(float valor, string tamanho, string nome, int quantidade,string categoria,string ator ,string genero){
  set_valor(valor);
  set_tamanho(tamanho);
  set_nome(nome);
  set_quantidade(quantidade);
  set_categoria(categoria);
  set_ator(ator);
  set_genero(genero);
}

filmes::~filmes(){

}
void filmes::set_ator(string ator){
  this->ator = ator;
}
string filmes::get_ator(){
  return ator;
}
void filmes::set_genero(string genero){
  this->genero = genero;
}
string filmes::get_genero(){
  return genero;
}

void filmes::imprime_quadro(){
  cout << "Nome: "<< get_nome() << endl;
  cout << "Quantidade: "<< get_quantidade() << endl;
  cout << "Tamanho: "<< get_tamanho() << " cm" << endl;
  cout << "Preço: "<< get_valor() <<" R$"<< endl;
  cout << "Categorias: "<< get_categoria() << endl;
  cout << "Genero: "<< get_genero() << endl;
  cout << "Ator: "<< get_ator() << endl;

}
