#include "menus.hpp"

menus::menus(){
}

menus::~menus(){
}

void menus::apresentamenu(int menu_escolhido){
  switch (menu_escolhido) {
    case 0:
      system("clear");
      cout << "|| Loja de quadros Nerd/Geek ||" << endl;
      cout << "(1) Modo Venda" << endl;
      cout << "(2) Modo Estoque" << endl;
      cout << "(3) Modo Recomendação" << endl;
      cout << "(0) Sair" << endl;
      cout << ":" << endl;
      break;
    case 1:
      system("clear");
      cout << "|| Modo Venda ||" << endl;
      cout << "(1) Escolha de produtos" << endl;
      cout << "(2) Lista Carrinho" << endl;
      cout << "(3) Finalizar Compra" << endl;
      cout << "(0) Voltar" << endl;
      cout << ":" << endl;
      break;
    case 2:
      system("clear");
      cout << "|| Modo Estoque ||" << endl;
      cout << "(1) Cadastro novo quadro" << endl;
      cout << "(2) Atualização de estoque" << endl;
      cout << "(3) Criação de nova categoria" << endl;
      cout << "(4) Lista de categorias" << endl;
      cout << "(0) Voltar" << endl;
      cout << ":" << endl;
      break;
    case 3:
      system("clear");
      cout << "|| Modo Recomendação ||" << endl;
      cout << "(1) Inserir dados do cliente" << endl;
      cout << "(0) Voltar" << endl;
      cout << ":" << endl;
      break;

  }
}
