#include "quadros.hpp"
#include <iostream>
#include <string>
#include <vector>


quadros::quadros(){
 valor = 0.0;
 tamanho = "0x0";
 nome = "Sem nome";
 quantidade = 0;
 categoria ="Sem categoria";
}

quadros::quadros(float valor, string tamanho, string nome, int quantidade, string categoria){
  set_valor(valor);
  set_tamanho(tamanho);
  set_nome(nome);
  set_quantidade(quantidade);
  set_categoria(categoria);
}
quadros::~quadros(){

}

void quadros::set_quantidade(int quantidade){
  this->quantidade = quantidade;
}

int quadros::get_quantidade(){
  return quantidade;
}

void quadros::set_valor(float valor){
  this->valor = valor;
}

float quadros::get_valor(){
  return valor;
}

void quadros::set_tamanho(string tamanho){
  this->tamanho= tamanho;
}

string quadros::get_tamanho(){
  return tamanho;
}

void quadros::set_nome(string nome){
  this->nome = nome;
}

string quadros::get_nome(){
  return nome;
}


void quadros::set_categoria(string categoria){
  this->categoria = categoria;
}

string quadros::get_categoria(){
  return categoria;
}

void quadros::imprime_quadro(){
  cout << "Nome: "<< get_nome() << endl;
  cout << "Quantidade: "<< get_quantidade() << endl;
  cout << "Tamanho: "<< get_tamanho() << " cm" << endl;
  cout << "Preço: "<< get_valor() <<" R$"<< endl;
  cout << "Categorias: "<< get_categoria() << endl;

}
