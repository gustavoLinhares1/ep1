#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "validacao.hpp"
#include "menus.hpp"
#include "quadros.hpp"
#include "jogos.hpp"
#include "filmes.hpp"

using namespace std;

int main(){
    int comando = -1;
    Validacao_dados valida;
    menus menu;
    jogos jogos_teste;
    filmes  filmes_teste;

    int escolha_quadro, escolha_categoria;


    // quadros
    int quantidade;
    float valor;
    string tamanho;
    string nome;
    string categoria;
    vector<int> escolhe_categoria;

    // filmes
    string ator;
    string genero;

    // jogos
    string plataforma;
    string personagem;

    while(comando != 0){
        menu.apresentamenu(0);
        comando = valida.valida_int(comando);
        switch(comando){
          case 1:
              while(comando!= 0){
                menu.apresentamenu(1);
                comando = valida.valida_int(comando);
              }
              comando = -1;
              break;
          case 2:
              while(comando!= 0){
                menu.apresentamenu(2);
                comando = valida.valida_int(comando);
                switch (comando) {
                  case 1:{
                    cout << "Deseja criar um quadro de jogo (1) ou um quadro de filme (2)?" << endl;
                    escolha_quadro = valida.valida_int(escolha_quadro);
                    while(escolha_quadro != 1 && escolha_quadro != 2){
                      cout << "Valor inválido. Digite novamente: " << endl;
                      escolha_quadro = valida.valida_int(escolha_quadro);
                    }
                    cout << "Quantidade: ";
                    quantidade = valida.valida_int(quantidade);
                    cout << "Valor: ";
                    valor = valida.valida_float(valor);
                    cout << "Tamanho: ";
                    tamanho = valida.valida_string(tamanho);
                    cout << "Nome: ";
                    nome = valida.valida_string(nome);
                    if (escolha_quadro == 1){
                      menu.apresentamenu(2);
                      cout << "-- Dados do Jogo --" <<  endl;
                      cout << "Plataforma: ";
                      plataforma = valida.valida_string(plataforma);
                      cout << "Personagem: ";
                      personagem = valida.valida_string(personagem);
                    }
                    else{
                      menu.apresentamenu(2);
                      cout << "-- Dados do Filme --" <<  endl;
                      cout << "Ator: ";
                      ator = valida.valida_string(ator);
                      cout << "Gênero: ";
                      genero = valida.valida_string(genero);
                    }
                    escolha_categoria = -1;
                    while(escolha_categoria != 0){
                      menu.apresentamenu(2);
                      string line;
                      int contador = 1;
                      cout << "-- Lista de Categorias --"<< endl;
                      ifstream lista("categoria.txt");
                      if (lista.is_open()){
                        while(getline(lista,line)){
                          cout << contador<< " - "<< line << endl;
                          if (escolha_categoria == contador){
                            categoria.append(line);
                            categoria.append(", ");
                          }
                          contador++;
                        }
                        lista.close();
                      }
                      cout << "Digite o número de uma categoria, para sair digite 0:" << endl;
                      escolha_categoria = valida.valida_int(escolha_categoria);
                    }

                    if (escolha_quadro == 1){
                      jogos_teste = jogos(valor,tamanho,nome,quantidade,categoria,plataforma,personagem);
                      menu.apresentamenu(2);
                      jogos_teste.imprime_quadro();
                    }
                    else{
                      filmes_teste = filmes(valor,tamanho,nome,quantidade,categoria,ator,genero);
                      menu.apresentamenu(2);
                      filmes_teste.imprime_quadro();
                    }
                    cout << "Dados do Novo Quadro. Aperte Qualquer tecla." << endl;
                    getchar();
                    break;
                  }
                  case 2:{
                    break;
                  }
                  case 3:{
                    cout << "Categoria: ";
                    nome = valida.valida_string(nome);
                    int flag=0;
                    string linha;
                    ifstream leitura("categoria.txt");
                    if (leitura.is_open()){
                      while(getline(leitura,linha)){
                        if(nome.compare(linha)==0){
                          cout << "Categoria já existente!" << endl;
                          flag =1;
                          getchar();
                        }
                      }
                      leitura.close();
                    }

                    if (flag == 0){
                      ofstream escrita("categoria.txt" , ios::app);
                      if (escrita.is_open()){
                        escrita << nome << endl;
                        cout << "escrita realizada" << endl;
                        getchar();
                        escrita.close();
                      }
                    }
                    break;
                  }
                  case 4:{
                    string line;
                    int contador = 0;
                    cout << "Lista de Categorias"<< endl;
                    ifstream lista("categoria.txt");
                    if (lista.is_open()){
                      while(getline(lista,line)){
                        cout << contador<< " - "<< line << endl;
                        contador++;
                      }
                      getchar();
                      lista.close();
                    }
                    break;
                  }
                }
              }
              comando = -1;
              break;
          case 3:
            while(comando!= 0){
              menu.apresentamenu(3);
              comando = valida.valida_int(comando);
            }
            comando = -1;
            break;
          case 0:
            break;
        }
    }
    system("clear");
    return 0;
}
